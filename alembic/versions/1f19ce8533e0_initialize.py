"""
Revision ID: 1f19ce8533e0
Create Date: 2022-08-04 22:57:43.424791
Comment:
	CryptoBOT API management database initialization script
"""
from os import path
from re import sub
import sys
from alembic import op
import sqlalchemy as sa

sys.path.insert(0, sub(r"/alembic/.+\.py", "/src", path.realpath(__file__)))

import model

revision = '1f19ce8533e0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
	op.create_table(
		'access_right',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_access_right')),
		sa.UniqueConstraint('name', name=op.f('uq_access_right_name'))
	)
	op.create_table(
		'alert_script',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_alert_script'))
	)
	op.create_table(
		'asset',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('symbol', sa.String(), nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('image_ext', sa.String(), nullable=True),
		sa.Column('coingecko_id', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_asset')),
		sa.UniqueConstraint('symbol', 'name', name='uq_asset_symbol')
	)
	op.create_table(
		'role',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_role')),
		sa.UniqueConstraint('name', name=op.f('uq_role_name'))
	)
	op.create_table(
		'team',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_team')),
		sa.UniqueConstraint('name', name=op.f('uq_team_name'))
	)
	op.create_table(
		'user',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('nick', sa.String(), nullable=False),
		sa.Column('email', sa.String(), nullable=True),
		sa.Column('token_expiration', sa.Integer(), nullable=False),
		sa.Column('password', sa.String(), nullable=False),
		sa.Column('reset_token', sa.String(), nullable=True),
		sa.Column('invite_token', sa.String(), nullable=True),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_user')),
		sa.UniqueConstraint('email', name=op.f('uq_user_email')),
		sa.UniqueConstraint('nick', name=op.f('uq_user_nick'))
	)
	op.create_table(
		'alert',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('user_id', sa.Integer(), nullable=True),
		sa.Column('team_id', sa.Integer(), nullable=True),
		sa.ForeignKeyConstraint(['team_id'], ['team.id'], name=op.f('fk_alert_team_id_team')),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_alert_user_id_user')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_alert'))
	)
	op.create_table(
		'authentication',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('user_id', sa.Integer(), nullable=True),
		sa.Column('key_hash', sa.String(), nullable=False),
		sa.Column('device_hash', sa.String(), nullable=False),
		sa.Column('device_name', sa.String(), nullable=True),
		sa.Column('expiration', sa.DateTime(), nullable=False),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_authentication_user_id_user')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_authentication')),
		sa.UniqueConstraint('device_hash', name=op.f('uq_authentication_device_hash'))
	)
	op.create_index(op.f('ix_authentication_user_id'), 'authentication', ['user_id'], unique=False)
	op.create_table(
		'funds',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('asset_id', sa.Integer(), nullable=False),
		sa.Column('amount', sa.Float(), nullable=False),
		sa.Column('locked', sa.Boolean(), nullable=False),
		sa.ForeignKeyConstraint(['asset_id'], ['asset.id'], name=op.f('fk_funds_asset_id_asset')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_funds'))
	)
	op.create_table(
		'indicator',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('public', sa.Boolean(), nullable=False),
		sa.Column('user_id', sa.Integer(), nullable=True),
		sa.Column('team_id', sa.Integer(), nullable=True),
		sa.CheckConstraint('(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)', name=op.f('ck_indicator_ck_indicator_user_or_team')),
		sa.ForeignKeyConstraint(['team_id'], ['team.id'], name=op.f('fk_indicator_team_id_team')),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_indicator_user_id_user')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_indicator')),
		sa.UniqueConstraint('name', 'user_id', 'team_id', name='uq_indicator_name')
	)
	op.create_table(
		'pair',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('symbol', sa.String(), nullable=False),
		sa.Column('base_asset_id', sa.Integer(), nullable=False),
		sa.Column('quote_asset_id', sa.Integer(), nullable=True),
		sa.ForeignKeyConstraint(['base_asset_id'], ['asset.id'], name=op.f('fk_pair_base_asset_id_asset')),
		sa.ForeignKeyConstraint(['quote_asset_id'], ['asset.id'], name=op.f('fk_pair_quote_asset_id_asset')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_pair')),
		sa.UniqueConstraint('base_asset_id', 'quote_asset_id', name='uq_pair_base_asset_id')
	)
	op.create_table(
		'role_access_right',
		sa.Column('role_id', sa.Integer(), nullable=False),
		sa.Column('right_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['right_id'], ['access_right.id'], name=op.f('fk_role_access_right_right_id_access_right')),
		sa.ForeignKeyConstraint(['role_id'], ['role.id'], name=op.f('fk_role_access_right_role_id_role')),
		sa.PrimaryKeyConstraint('role_id', 'right_id', name=op.f('pk_role_access_right'))
	)
	op.create_table(
		'service',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('state', model.base.IntEnumSQL(model.service.State), nullable=False),
		sa.Column('type', model.base.IntEnumSQL(model.service.ServiceType), nullable=False),
		sa.Column('host', sa.String(), nullable=False),
		sa.Column('port', sa.Integer(), nullable=False),
		sa.Column('server_secret', sa.String(), nullable=True),
		sa.Column('user_id', sa.Integer(), nullable=True),
		sa.Column('team_id', sa.Integer(), nullable=True),
		sa.CheckConstraint('(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)', name=op.f('ck_service_ck_service_user_or_team')),
		sa.ForeignKeyConstraint(['team_id'], ['team.id'], name=op.f('fk_service_team_id_team')),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_service_user_id_user')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_service')),
		sa.UniqueConstraint('host', 'port', name='uq_service_host'),
		sa.UniqueConstraint('name', 'user_id', 'team_id', name='uq_service_name')
	)
	op.create_table(
		'strategy',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('name', sa.String(), nullable=False),
		sa.Column('public', sa.Boolean(), nullable=False),
		sa.Column('user_id', sa.Integer(), nullable=True),
		sa.Column('team_id', sa.Integer(), nullable=True),
		sa.CheckConstraint('(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)', name=op.f('ck_strategy_ck_strategy_user_or_team')),
		sa.ForeignKeyConstraint(['team_id'], ['team.id'], name=op.f('fk_strategy_team_id_team')),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_strategy_user_id_user')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_strategy')),
		sa.UniqueConstraint('name', 'user_id', 'team_id', name='uq_strategy_name')
	)
	op.create_table(
		'team_user',
		sa.Column('team_id', sa.Integer(), nullable=False),
		sa.Column('user_id', sa.Integer(), nullable=False),
		sa.Column('role_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['role_id'], ['role.id'], name=op.f('fk_team_user_role_id_role')),
		sa.ForeignKeyConstraint(['team_id'], ['team.id'], name=op.f('fk_team_user_team_id_team')),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_team_user_user_id_user')),
		sa.PrimaryKeyConstraint('team_id', 'user_id', name=op.f('pk_team_user'))
	)
	op.create_index(op.f('ix_team_user_team_id'), 'team_user', ['team_id'], unique=False)
	op.create_index(op.f('ix_team_user_user_id'), 'team_user', ['user_id'], unique=False)
	op.create_table(
		'alert_notification',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('alert_id', sa.Integer(), nullable=False),
		sa.Column('pair_id', sa.Integer(), nullable=True),
		sa.Column('type', sa.String(), nullable=False),
		sa.Column('data', sa.String(), nullable=True),
		sa.ForeignKeyConstraint(['alert_id'], ['alert.id'], name=op.f('fk_alert_notification_alert_id_alert')),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_alert_notification_pair_id_pair')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_alert_notification'))
	)
	op.create_table(
		'alert_pair',
		sa.Column('alert_id', sa.Integer(), nullable=False),
		sa.Column('pair_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['alert_id'], ['alert.id'], name=op.f('fk_alert_pair_alert_id_alert')),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_alert_pair_pair_id_pair')),
		sa.PrimaryKeyConstraint('alert_id', 'pair_id', name=op.f('pk_alert_pair'))
	)
	op.create_table(
		'alert_rule',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('alert_id', sa.Integer(), nullable=False),
		sa.Column('script_id', sa.Integer(), nullable=False),
		sa.Column('collation_symbol', sa.String(), nullable=False),
		sa.Column('collation_target', sa.String(), nullable=False),
		sa.ForeignKeyConstraint(['alert_id'], ['alert.id'], name=op.f('fk_alert_rule_alert_id_alert')),
		sa.ForeignKeyConstraint(['script_id'], ['alert_script.id'], name=op.f('fk_alert_rule_script_id_alert_script')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_alert_rule'))
	)
	op.create_table(
		'indicator_version',
		sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
		sa.Column('indicator_id', sa.Integer(), nullable=False),
		sa.Column('major', sa.Integer(), nullable=False),
		sa.Column('minor', sa.Integer(), nullable=False),
		sa.Column('revision', sa.Integer(), nullable=False),
		sa.Column('code', sa.Text(), nullable=False),
		sa.Column('requirements', sa.ARRAY(sa.String()), nullable=True),
		sa.Column('created', sa.DateTime(), nullable=False),
		sa.Column('created_by_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['created_by_id'], ['user.id'], name=op.f('fk_indicator_version_created_by_id_user')),
		sa.ForeignKeyConstraint(['indicator_id'], ['indicator.id'], name=op.f('fk_indicator_version_indicator_id_indicator')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_indicator_version')),
		sa.UniqueConstraint('indicator_id', 'major', 'minor', 'revision', name='uq_indicator_version_indicator_id')
	)
	op.create_table(
		'strategy_version',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('strategy_id', sa.BigInteger(), nullable=False),
		sa.Column('major', sa.Integer(), nullable=False),
		sa.Column('minor', sa.Integer(), nullable=False),
		sa.Column('revision', sa.Integer(), nullable=False),
		sa.Column('code', sa.Text(), nullable=False),
		sa.Column('requirements', sa.ARRAY(sa.String()), nullable=True),
		sa.Column('created', sa.DateTime(), nullable=False),
		sa.Column('created_by_id', sa.Integer(), nullable=False),
		sa.ForeignKeyConstraint(['created_by_id'], ['user.id'], name=op.f('fk_strategy_version_created_by_id_user')),
		sa.ForeignKeyConstraint(['strategy_id'], ['strategy.id'], name=op.f('fk_strategy_version_strategy_id_strategy')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_strategy_version')),
		sa.UniqueConstraint('strategy_id', 'major', 'minor', 'revision', name='uq_strategy_version_strategy_id')
	)
	op.create_table(
		'test',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('service_id', sa.Integer(), nullable=True),
		sa.Column('date_start', sa.DateTime(), nullable=True),
		sa.Column('date_stop', sa.DateTime(), nullable=True),
		sa.Column('user_id', sa.Integer(), nullable=True),
		sa.Column('team_id', sa.Integer(), nullable=True),
		sa.Column('created', sa.DateTime(), nullable=False),
		sa.Column('finished', sa.DateTime(), nullable=True),
		sa.Column('created_by_id', sa.Integer(), nullable=False),
		sa.CheckConstraint('(service_id IS NOT NULL) OR (finished IS NOT NULL)', name=op.f('ck_test_ck_test_unfinished_test_service')),
		sa.CheckConstraint('(user_id IS NULL OR team_id IS NULL) AND (user_id IS NOT NULL OR team_id IS NOT NULL)', name=op.f('ck_test_ck_test_user_or_team')),
		sa.ForeignKeyConstraint(['created_by_id'], ['user.id'], name=op.f('fk_test_created_by_id_user')),
		sa.ForeignKeyConstraint(['service_id'], ['service.id'], name=op.f('fk_test_service_id_service')),
		sa.ForeignKeyConstraint(['team_id'], ['team.id'], name=op.f('fk_test_team_id_team')),
		sa.ForeignKeyConstraint(['user_id'], ['user.id'], name=op.f('fk_test_user_id_user')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_test'))
	)
	op.create_table(
		'position',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('type', sa.Enum('LONG', 'SHORT', name='type'), nullable=False),
		sa.Column('pair_id', sa.Integer(), nullable=False),
		sa.Column('funds_id', sa.BigInteger(), nullable=False),
		sa.Column('status', model.base.IntEnumSQL(model.position.Status), nullable=False),
		sa.Column('open_amount', sa.Float(), nullable=False),
		sa.Column('close_amount', sa.Float(), nullable=True),
		sa.Column('open_price', sa.Float(), nullable=False),
		sa.Column('close_price', sa.Float(), nullable=True),
		sa.Column('open_time', sa.DateTime(), nullable=False),
		sa.Column('close_time', sa.DateTime(), nullable=True),
		sa.Column('profit', sa.Float(), nullable=True),
		sa.Column('strategy_version_id', sa.BigInteger(), nullable=False),
		sa.Column('order_id', sa.BigInteger(), nullable=True),
		sa.ForeignKeyConstraint(['funds_id'], ['funds.id'], name=op.f('fk_position_funds_id_funds')),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_position_pair_id_pair')),
		sa.ForeignKeyConstraint(['strategy_version_id'], ['strategy_version.id'], name=op.f('fk_position_strategy_version_id_strategy_version')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_position'))
	)
	op.create_table(
		'test_funds',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('test_id', sa.BigInteger(), autoincrement=True, nullable=True),
		sa.Column('asset_id', sa.Integer(), nullable=True),
		sa.Column('amount', sa.Float(), nullable=False),
		sa.Column('locked', sa.Boolean(), nullable=False),
		sa.ForeignKeyConstraint(['asset_id'], ['asset.id'], name=op.f('fk_test_funds_asset_id_asset')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_funds_test_id_test')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_test_funds'))
	)
	op.create_table(
		'test_strategy_version',
		sa.Column('test_id', sa.BigInteger(), nullable=False),
		sa.Column('strategy_version_id', sa.BigInteger(), nullable=False),
		sa.ForeignKeyConstraint(['strategy_version_id'], ['strategy_version.id'], name=op.f('fk_test_strategy_version_strategy_version_id_strategy_version')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_strategy_version_test_id_test')),
		sa.PrimaryKeyConstraint('test_id', 'strategy_version_id', name=op.f('pk_test_strategy_version'))
	)
	op.create_table(
		'test_position',
		sa.Column('id', sa.BigInteger(), autoincrement=True, nullable=False),
		sa.Column('type', sa.Enum('LONG', 'SHORT', name='type'), nullable=False),
		sa.Column('test_id', sa.BigInteger(), nullable=True),
		sa.Column('pair_id', sa.Integer(), nullable=False),
		sa.Column('funds_id', sa.BigInteger(), nullable=False),
		sa.Column('status', model.base.IntEnumSQL(model.position.Status), nullable=False),
		sa.Column('open_amount', sa.Float(), nullable=False),
		sa.Column('close_amount', sa.Float(), nullable=True),
		sa.Column('open_price', sa.Float(), nullable=False),
		sa.Column('close_price', sa.Float(), nullable=True),
		sa.Column('open_time', sa.DateTime(), nullable=False),
		sa.Column('close_time', sa.DateTime(), nullable=True),
		sa.Column('profit', sa.Float(), nullable=True),
		sa.Column('strategy_version_id', sa.BigInteger(), nullable=False),
		sa.Column('order_id', sa.BigInteger(), nullable=True),
		sa.ForeignKeyConstraint(['funds_id'], ['test_funds.id'], name=op.f('fk_test_position_funds_id_test_funds')),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_test_position_pair_id_pair')),
		sa.ForeignKeyConstraint(['strategy_version_id'], ['strategy_version.id'], name=op.f('fk_test_position_strategy_version_id_strategy_version')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_position_test_id_test')),
		sa.PrimaryKeyConstraint('id', name=op.f('pk_test_position'))
	)
	op.create_table(
		'test_pair',
		sa.Column('test_id', sa.BigInteger(), nullable=False),
		sa.Column('pair_id', sa.BigInteger(), nullable=False),
		sa.ForeignKeyConstraint(['pair_id'], ['pair.id'], name=op.f('fk_test_pair_pair_id_pair')),
		sa.ForeignKeyConstraint(['test_id'], ['test.id'], name=op.f('fk_test_pair_test_id_test')),
		sa.PrimaryKeyConstraint('test_id', 'pair_id', name=op.f('pk_test_pair'))
	)


def downgrade():
	op.drop_table('test_position')
	op.drop_table('test_strategy_version')
	op.drop_table('test_funds')
	op.drop_table('position')
	op.drop_table('test')
	op.drop_table('strategy_version')
	op.drop_table('indicator_version')
	op.drop_table('alert_rule')
	op.drop_table('alert_pair')
	op.drop_table('alert_notification')
	op.drop_index(op.f('ix_team_user_user_id'), table_name='team_user')
	op.drop_index(op.f('ix_team_user_team_id'), table_name='team_user')
	op.drop_table('team_user')
	op.drop_table('strategy')
	op.drop_table('service')
	op.drop_table('role_access_right')
	op.drop_table('pair')
	op.drop_table('indicator')
	op.drop_table('funds')
	op.drop_index(op.f('ix_authentication_user_id'), table_name='authentication')
	op.drop_table('authentication')
	op.drop_table('alert')
	op.drop_table('user')
	op.drop_table('team')
	op.drop_table('role')
	op.drop_table('asset')
	op.drop_table('alert_script')
	op.drop_table('access_right')
	op.drop_table('test_pair')
