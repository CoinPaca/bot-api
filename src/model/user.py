from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship, reconstructor
from .base import Base
from .team import TeamUser
from .role import Role, AccessRight, RoleAccessRight


class User(Base):
	__slots__ = ("_token_count", "_rights", "_team_ids")
	__tablename__ = 'user'

	id = Column(Integer, primary_key=True, autoincrement=True)
	nick = Column(String, nullable=False, unique=True)
	email = Column(String, nullable=True, unique=True)
	token_expiration = Column(Integer, nullable=False, default=8640)
	password = Column(String, nullable=False)
	reset_token = Column(String, nullable=True)
	invite_token = Column(String, nullable=True)

	@reconstructor
	def __init__(self, *args, **kwargs):
		super(User, self).__init__(*args, **kwargs)
		self._token_count = 0
		self._rights = {}
		self._team_ids = []

	@property
	def token_count(self):
		return self._token_count

	@token_count.setter
	def token_count(self, val: int):
		self._token_count = val

	@property
	def team_ids(self):
		if not self._team_ids:
			team_users = (
				TeamUser.query
				.join(Role, Role.id == TeamUser.role_id)
				.join(RoleAccessRight, RoleAccessRight.role_id == Role.id)
				.join(AccessRight, RoleAccessRight.id == RoleAccessRight.right_id)
				.filter(TeamUser.user_id == self.id)
				.all()
			)
			for tu in team_users:
				self._team_ids.append(tu.team_id)
				self._rights[tu.team_id] = [right.name for right in tu.role.rights]
		return self._team_ids

	@property
	def rights(self):
		return self._rights


class Authentication(Base):
	__tablename__ = 'authentication'

	id = Column(Integer, primary_key=True, autoincrement=True)
	user_id = Column(Integer, ForeignKey('user.id'), index=True)
	key_hash = Column(String, nullable=False)
	device_hash = Column(String, nullable=False, unique=True)
	device_name = Column(String, nullable=True)
	expiration = Column(DateTime, nullable=False)

	user = relationship('User')
