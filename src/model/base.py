from sqlalchemy import MetaData, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import TypeDecorator


Base = declarative_base(metadata=MetaData(naming_convention={
	"ix": "ix_%(column_0_label)s",
	"uq": "uq_%(table_name)s_%(column_0_name)s",
	"ck": "ck_%(table_name)s_%(constraint_name)s",
	"fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
	"pk": "pk_%(table_name)s"
}))


class IntEnumSQL(TypeDecorator):
	impl = Integer
	cache_ok = True

	def __init__(self, enumtype, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._enumtype = enumtype

	def process_bind_param(self, value, dialect):
		return value

	def process_result_value(self, value, dialect):
		return self._enumtype(value)
