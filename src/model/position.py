from enum import IntEnum, Enum
from sqlalchemy import (
	Column, Boolean, Integer, BigInteger, Float, ForeignKey, Enum as EnumSQL, DateTime
)
from sqlalchemy.orm import relationship
from .base import Base, IntEnumSQL
from .asset import Pair


class Type(Enum):
	LONG = "LONG"
	SHORT = "SHORT"


class Status(IntEnum):
	OPENING = 0
	OPEN = 1
	CLOSING = 2
	CLOSED = 3
	CANCELED = 4


class Position(Base):
	__tablename__ = "position"

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	type = Column(EnumSQL(Type), nullable=False)
	pair_id = Column(Integer, ForeignKey("pair.id"), nullable=False)
	funds_id = Column(BigInteger, ForeignKey("funds.id"), nullable=False)
	status = Column(IntEnumSQL(Status), nullable=False)
	open_amount = Column(Float, nullable=False)
	close_amount = Column(Float, nullable=True)
	open_price = Column(Float, nullable=False)
	close_price = Column(Float, nullable=True)
	open_time = Column(DateTime, nullable=False)
	close_time = Column(DateTime, nullable=True)
	profit = Column(Float, nullable=True)
	strategy_version_id = Column(BigInteger, ForeignKey("strategy_version.id"), nullable=False)
	order_id = Column(BigInteger, nullable=True)

	pair = relationship(Pair)
	funds = relationship("Funds")
	strategy_version = relationship("StrategyVersion")


class Funds(Base):
	__tablename__ = "funds"

	id = Column(BigInteger, primary_key=True, autoincrement=True)
	asset_id = Column(Integer, ForeignKey("asset.id"), nullable=False)
	amount = Column(Float, nullable=False)
	locked = Column(Boolean, nullable=False, default=False)

	asset = relationship("Asset")
