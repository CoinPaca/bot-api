from weepy import Route
from lib.auth import Auth


class User:
	@staticmethod
	@Route("/login", methods=["POST"])
	async def login(req, resp):
		username = req.data.get("username")
		password = req.data.get("password")
		device = req.data.get("device_name")
		if username and password and (auth := Auth.create_token(username, password, device)):
			return {"status": True, "id": auth[0], "device": auth[2], "key": auth[1]}
		return {"status": False, "code": "U100", "message": "Invalid credentials"}
