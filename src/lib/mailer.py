from re import compile
from smtplib import SMTP_SSL
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mailer:
	smtp_server = None
	smtp_port = None
	address = None
	password = None

	@staticmethod
	def is_valid(email):
		r = compile(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b')
		return r.match(email)

	def init(smtp_server, smtp_port, address, password):
		Mailer.smtp_server = smtp_server
		Mailer.smtp_port = smtp_port
		Mailer.address = address
		Mailer.password = password

	def send(recipient, subject="", message="", mime="text/plain"):
		smtp = SMTP_SSL(Mailer.smtp_server, Mailer.smtp_port)
		smtp.ehlo()
		smtp.login(Mailer.email, Mailer.password)
		multipart = MIMEMultipart()
		multipart['From'] = Mailer.email
		multipart['To'] = recipient
		multipart['Subject'] = subject
		if mime == "text/html":
			multipart.attach(MIMEText(message, 'html'))
		else:
			multipart.attach(MIMEText(message, 'plain'))
		smtp.sendmail(Mailer.email, recipient, multipart.as_string())
		smtp.quit()
